import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:flutter_meet/src/ui/google_map.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Location location = Location();
  dynamic userLocation;
  bool _permission = false;

  @override
  void initState() {
    super.initState();
    _updateLocation();
  }

  void _navigatePage(context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => GoogleMaps(userLocation:userLocation)),
    );
  }

  void _updateLocation() {
    _getLocation().then((value) {
      setState(() {
        userLocation = value;
        print('*****userLocation***** $value');
      });
    });
  }

  Future<dynamic> _getLocation() async {
    dynamic currentLocation;

    try {
      bool serviceStatus = await location.serviceEnabled();
      print("Service status: $serviceStatus");
      if (serviceStatus) {
        _permission = await location.requestPermission();
        print("Permission: $_permission");
        if (_permission) {
          dynamic status = await location.requestPermission();
          print('***status*** $status ${await location.hasPermission()}');
          currentLocation = await location.getLocation();
        } else {
          bool serviceStatusResult = await location.requestService();
          print("Service status activated after request: $serviceStatusResult");
        }
      }
    } catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        print('*****PERMISSION_DENIED***** ${e.message}');
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        print('*****SERVICE_STATUS_ERROR***** ${e.message}');
      } else
        print('*****ERROR***** $e');
      currentLocation = null;
    }

    return currentLocation;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Location: ',
              style: Theme.of(context).textTheme.display1,
            ),
            userLocation == null
                ? CircularProgressIndicator()
                : Text(
                    '${userLocation.latitude.toString()}, ${userLocation.longitude.toString()}.',
                    style: Theme.of(context).textTheme.display1,
                  ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _navigatePage(context),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
